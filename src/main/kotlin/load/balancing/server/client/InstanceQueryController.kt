package load.balancing.server.client

import load.balancing.server.application.LoadBalancingAlgorithmType
import load.balancing.server.application.TaskType
import load.balancing.server.application.port.InstanceQueryPort
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/dictionary")
class InstanceQueryController(
    private val instanceQueryPort: InstanceQueryPort
) {

  @GetMapping("/task-types")
  fun fetchTaskTypes(): List<TaskType> {
    return instanceQueryPort.fetchTaskTypes()
  }

  @GetMapping("/algorithms")
  fun fetchAlgorithms(): List<LoadBalancingAlgorithmType> {
    return instanceQueryPort.fetchAlgorithms()
  }

}
