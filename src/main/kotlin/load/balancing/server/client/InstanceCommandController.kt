package load.balancing.server.client

import load.balancing.server.application.command.InstanceCommand
import load.balancing.server.application.port.InstanceCommandPort
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/instances")
class InstanceCommandController(private val port: InstanceCommandPort) {

    @PostMapping("/task/invoke")
    fun invokeTask(@RequestBody command: InstanceCommand) {
        port.invokeTask(command)
    }

}