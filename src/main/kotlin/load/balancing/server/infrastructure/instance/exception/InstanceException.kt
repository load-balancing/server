package load.balancing.server.infrastructure.instance.exception

class InstanceException(message: String, cause: Throwable) : RuntimeException(message, cause)