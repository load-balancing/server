package load.balancing.server.infrastructure.instance.response

import java.time.Duration

data class TaskResponse(
        val duration: Duration
)
