package load.balancing.server.infrastructure.instance

import com.netflix.appinfo.InstanceInfo
import com.netflix.discovery.EurekaClient
import com.netflix.discovery.shared.Application
import load.balancing.server.application.command.TaskCommand
import load.balancing.server.application.projection.Instance
import load.balancing.server.configuration.CustomProperties
import load.balancing.server.infrastructure.instance.exception.InstanceException
import load.balancing.server.infrastructure.instance.port.InstanceInterfacePort
import load.balancing.server.infrastructure.instance.response.TaskLoadResponse
import load.balancing.server.infrastructure.instance.response.TaskResponse
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForObject
import org.springframework.web.client.postForObject

@Async
@Service
class InstanceInterfaceAdapter(
    private val customProperties: CustomProperties,
    @Qualifier("eurekaClient") private val eurekaClient: EurekaClient
) : InstanceInterfacePort {

  private val restTemplate = RestTemplate()

  override fun calculateFactorial(instance: Instance, command: TaskCommand): TaskResponse {
    var url = ""
    try {
      val eurekaInstance = getInstanceFromEureka(instance.instanceId)
      url = composeUrl(eurekaInstance, "task/factorial")
      return executeTask(url, command)
    } catch (exception: Exception) {
      throw InstanceException("Instance error for url = $url", exception)
    }
  }

  override fun getInstanceIds(): List<String> {
    val app = eurekaClient.getApplication(customProperties.workerName) ?: return emptyList()
    val instances = app.instances
    return instances.map { it.instanceId }
  }

  override fun getInstanceLoad(instanceId: String): Instance {
    val eurekaInstance = getInstanceFromEureka(instanceId)
    val url = composeUrl(eurekaInstance, "load")
    return restTemplate.getForObject<TaskLoadResponse>(url, TaskLoadResponse::class)
  }

  private fun executeTask(url: String, command: TaskCommand): TaskResponse {
    return restTemplate.postForObject(url, command, TaskResponse::class)
  }

  private fun getInstanceFromEureka(instanceId: String): InstanceInfo {
    return eurekaClient.getApplication(customProperties.workerName)
        .getByInstanceId(instanceId)
  }

  private fun composeUrl(instance: InstanceInfo, restUrl: String): String {
    return "http://${instance.ipAddr}:${instance.port}/${restUrl}"
  }

}