package load.balancing.server.infrastructure.instance.port

import load.balancing.server.application.command.TaskCommand
import load.balancing.server.application.projection.Instance
import load.balancing.server.infrastructure.instance.response.TaskLoadResponse
import load.balancing.server.infrastructure.instance.response.TaskResponse

interface InstanceInterfacePort {
    fun calculateFactorial(instance: Instance, command: TaskCommand): TaskResponse
    fun getInstanceIds(): List<String>
    fun getInstanceLoad(instanceId: String): Instance
}
