package load.balancing.server.infrastructure.instance.response

import load.balancing.server.application.projection.Instance
import java.math.BigDecimal
import java.math.MathContext

data class TaskLoadResponse(override var instanceId: String = "", override var cpuLoad: BigDecimal = BigDecimal.ZERO) : Instance {
  init {
    this.cpuLoad = cpuLoad.round(MathContext.DECIMAL32)
  }
}
