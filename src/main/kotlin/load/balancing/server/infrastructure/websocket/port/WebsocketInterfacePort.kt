package load.balancing.server.infrastructure.websocket.port

import load.balancing.server.application.projection.Instance

interface WebsocketInterfacePort {
  fun publishInstanceLoad(load: Instance)
}