package load.balancing.server.infrastructure.websocket

import load.balancing.server.application.projection.Instance
import load.balancing.server.infrastructure.websocket.port.WebsocketInterfacePort
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Service

@Service
internal class WebsocketInterfaceAdapter(
    private val simpMessagingTemplate: SimpMessagingTemplate
) : WebsocketInterfacePort {

  override fun publishInstanceLoad(load: Instance) {
    simpMessagingTemplate.convertAndSend("/topic/load", load)
  }

}