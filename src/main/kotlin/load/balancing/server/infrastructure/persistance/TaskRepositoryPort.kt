package load.balancing.server.infrastructure.persistance

import load.balancing.server.application.Task
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface TaskRepositoryPort : JpaRepository<Task, String>

