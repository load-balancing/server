package load.balancing.server.application

import load.balancing.server.application.command.InstanceCommand
import load.balancing.server.application.command.TaskCommand
import load.balancing.server.infrastructure.instance.port.InstanceInterfacePort
import java.util.*

internal class LoadBalancingFactory(
    private val instanceInterfacePort: InstanceInterfacePort
) {

  fun prepareAlgorithm(command: InstanceCommand, task: ExecutableTask): LoadBalancingAlgorithm = when (command.algorithm) {
    LoadBalancingAlgorithmType.ROUND_ROBIN -> RoundRobin(task, command.totalTasksCount)
  }

  fun prepareTask(command: InstanceCommand): ExecutableTask = when (command.taskType) {
    TaskType.FACTORIAL -> FactorialTask(instanceInterfacePort, createTaskCommand(command))
  }

  private fun createTaskCommand(instanceCommand: InstanceCommand) = TaskCommand(
      UUID.randomUUID(),
      instanceCommand.tasksPerInstance
  )

}