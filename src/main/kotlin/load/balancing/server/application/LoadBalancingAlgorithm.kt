package load.balancing.server.application

import load.balancing.server.application.projection.Instance

internal abstract class LoadBalancingAlgorithm(
    protected val task: ExecutableTask,
    protected val totalTaskCount: Int
) {
  abstract fun invokeTask(instances: List<Instance>)
}