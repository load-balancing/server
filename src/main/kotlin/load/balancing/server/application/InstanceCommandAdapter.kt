package load.balancing.server.application

import load.balancing.server.application.command.InstanceCommand
import load.balancing.server.application.port.InstanceCommandPort
import load.balancing.server.infrastructure.instance.port.InstanceInterfacePort
import load.balancing.server.infrastructure.persistance.TaskRepositoryPort
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import java.time.LocalDateTime
import java.util.*
import javax.annotation.PostConstruct

@Service
internal class InstanceCommandAdapter(
    private val instanceInterfacePort: InstanceInterfacePort,
    private val taskRepositoryPort: TaskRepositoryPort
) : InstanceCommandPort {

  private lateinit var factory: LoadBalancingFactory;

  @PostConstruct
  fun setUp() {
    factory = LoadBalancingFactory(instanceInterfacePort)
  }

  override fun invokeTask(command: InstanceCommand) {
    val task = factory.prepareTask(command)
    val algorithm = factory.prepareAlgorithm(command, task)

    Flux.fromIterable(instanceInterfacePort.getInstanceIds())
        .map { instanceInterfacePort.getInstanceLoad(it) }
        .collectList()
        .subscribe {
          persistTask()
          algorithm.invokeTask(it)
        }
  }

  private fun persistTask() {
    val task = Task(
        UUID.randomUUID().toString(),
        LocalDateTime.now()
    )
    taskRepositoryPort.save(task)
  }

}