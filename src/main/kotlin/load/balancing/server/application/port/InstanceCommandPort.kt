package load.balancing.server.application.port

import load.balancing.server.application.command.InstanceCommand

interface InstanceCommandPort {
    fun invokeTask(command: InstanceCommand)
}