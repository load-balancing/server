package load.balancing.server.application.port

import load.balancing.server.application.LoadBalancingAlgorithmType
import load.balancing.server.application.TaskType
import load.balancing.server.application.projection.Instance
import reactor.core.publisher.Flux

interface InstanceQueryPort {
  fun fetchInstancesLoad() : Flux<Instance>
  fun fetchTaskTypes(): List<TaskType>
  fun fetchAlgorithms(): List<LoadBalancingAlgorithmType>
}