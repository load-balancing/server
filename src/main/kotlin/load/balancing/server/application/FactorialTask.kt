package load.balancing.server.application

import load.balancing.server.application.command.TaskCommand
import load.balancing.server.application.projection.Instance
import load.balancing.server.infrastructure.instance.port.InstanceInterfacePort

class FactorialTask(
        private val port: InstanceInterfacePort,
        private val command: TaskCommand
) : ExecutableTask {

    override fun execute(instance: Instance) {
        port.calculateFactorial(instance, command)
    }

}