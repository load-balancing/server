package load.balancing.server.application

import load.balancing.server.application.port.InstanceQueryPort
import load.balancing.server.application.projection.Instance
import load.balancing.server.infrastructure.instance.port.InstanceInterfacePort
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import java.time.Duration

@Service
internal class InstanceQueryAdapter(
    private val instanceInterfacePort: InstanceInterfacePort
) : InstanceQueryPort {

  override fun fetchInstancesLoad(): Flux<Instance> {
    return Flux.interval(Duration.ofSeconds(1))
        .flatMapIterable { instanceInterfacePort.getInstanceIds() }
        .map { instanceInterfacePort.getInstanceLoad(it) }
  }

  override fun fetchTaskTypes(): List<TaskType> = TaskType.values().toList()

  override fun fetchAlgorithms(): List<LoadBalancingAlgorithmType> = LoadBalancingAlgorithmType.values().toList()

}