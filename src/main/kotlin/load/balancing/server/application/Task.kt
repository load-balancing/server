package load.balancing.server.application

import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class Task(

    @Id
    val id: String,

    @Column
    val timeStart: LocalDateTime,

    @Column
    val timeEnd: LocalDateTime? = null,

    @Column
    val duration: Int? = null
)
