package load.balancing.server.application

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import load.balancing.server.application.projection.Instance
import load.balancing.server.shared.logger

internal class RoundRobin(task: ExecutableTask, totalTaskCount: Int) : LoadBalancingAlgorithm(task, totalTaskCount) {

  override fun invokeTask(instances: List<Instance>) {
    logger.info { "Start round robin for ${instances.size} instances" }

    for (x in 0 until totalTaskCount) {
      GlobalScope.launch {
        val instance = instances[x % instances.size]
        logger.info { "Launching task on instance ${instance.instanceId}" }
        task.execute(instance)
      }
    }

  }

}