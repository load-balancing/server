package load.balancing.server.application.command

import load.balancing.server.application.LoadBalancingAlgorithmType
import load.balancing.server.application.TaskType

data class InstanceCommand (
        val tasksPerInstance: Int,
        val totalTasksCount: Int,
        val taskType: TaskType,
        val algorithm: LoadBalancingAlgorithmType
)
