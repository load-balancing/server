package load.balancing.server.application.command

import java.util.*

data class TaskCommand(
    val uuid: UUID,
    val taskCount: Int
)
