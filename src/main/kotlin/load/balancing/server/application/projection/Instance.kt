package load.balancing.server.application.projection

import java.math.BigDecimal

interface Instance {
    val instanceId: String
    val cpuLoad: BigDecimal
}