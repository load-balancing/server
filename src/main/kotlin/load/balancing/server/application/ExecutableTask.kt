package load.balancing.server.application

import load.balancing.server.application.projection.Instance

internal interface ExecutableTask {
    fun execute(instance: Instance);
}