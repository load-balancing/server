package load.balancing.server.application

import load.balancing.server.infrastructure.instance.port.InstanceInterfacePort
import load.balancing.server.infrastructure.websocket.port.WebsocketInterfacePort
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import java.time.Duration
import javax.annotation.PostConstruct

@Service
internal class LoadPublisher(
    private val websocketInterfacePort: WebsocketInterfacePort,
    private val instanceInterfacePort: InstanceInterfacePort
) {

  @PostConstruct
  fun initialize() {
    Flux.interval(Duration.ofSeconds(1))
        .flatMapIterable { instanceInterfacePort.getInstanceIds() }
        .map { instanceInterfacePort.getInstanceLoad(it) }
        .subscribe{
          websocketInterfacePort.publishInstanceLoad(it)
        }
  }

}