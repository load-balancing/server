package load.balancing.server.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties(prefix = "custom", ignoreUnknownFields = false)
class CustomProperties {
    final lateinit var workerName: String
}